# syntax=docker/dockerfile:1

FROM nvcr.io/nvidia/cuda:11.0.3-cudnn8-devel-ubuntu20.04 AS builder
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime
RUN echo "Installing Python and Pytorch..." && \
    apt-get update --fix-missing && \
    apt-get install -q -y --no-install-recommends \
        python3-dev \
        python3-pip \
        tzdata && \
    pip3 install torch torchvision torchaudio && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /workspace
RUN echo "Installing ROS Noetic..." && \
    apt-get update --fix-missing && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
        ros-noetic-desktop-full && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "Installing OpenPose..." && \
    apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
        build-essential \
        git \
        libatlas-base-dev \
        libboost-all-dev \
        libgflags-dev \
        libcanberra-gtk-module \
        libgoogle-glog-dev \
        libhdf5-serial-dev \
        libleveldb-dev \
        liblmdb-dev \
        libopencv-dev \
        libprotobuf-dev \
        libsnappy-dev \
        libviennacl-dev \
        ocl-icd-opencl-dev \
        opencl-headers \
        pciutils \
        protobuf-compiler \
        python3-setuptools && \
    pip3 install \
        numpy \
        opencv-python \
        protobuf && \
    rm -rf /var/lib/apt/lists/* && \
    cd /home && \
    git clone --depth 1 --branch 'v1.7.0' \
        https://github.com/CMU-Perceptual-Computing-Lab/openpose && \
    mkdir -p /home/openpose/build && cd /home/openpose/build && \
    cmake -DBUILD_PYTHON=ON .. && \
    sed -ie 's/set(AMPERE "80 86")/#&/g' ../cmake/Cuda.cmake && \
    sed -ie 's/set(AMPERE "80 86")/#&/g' ../3rdparty/caffe/cmake/Cuda.cmake && \
    make -j `nproc` && \
    make install

FROM builder AS compiler
SHELL ["/bin/bash", "-c"]
COPY ./entrypoint.sh ./entrypoint.sh
COPY ./ /home/spring_ws/src/ros_openpose/
COPY ./wp4_msgs /home/spring_ws/src/wp4_msgs/
RUN echo "Compiling packages..." && \
    chmod +x ./entrypoint.sh && \
    . /opt/ros/noetic/setup.bash && cd /home/spring_ws && catkin_make && \
    echo "source /home/spring_ws/devel/setup.bash" >> ~/.bashrc
EXPOSE 11311
ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]
