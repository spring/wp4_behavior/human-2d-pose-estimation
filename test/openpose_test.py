#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pathlib
import sys

file_path = pathlib.Path(__file__).parent.parent.parent.absolute()
sys.path.append(str(file_path))
print(sys.path)
import os
import unittest

import cv2
import numpy as np

# Ros libraries
import roslib
import rospy
from PIL import Image
from ros_openpose.msg import BodyPart, Frame, Person
from sensor_msgs.msg import CompressedImage

# Ros Messages
from std_msgs.msg import Header
from wp4_msgs.msg import BoxDetection, BoxDetectionFrame


class TestOpenPose(unittest.TestCase):
    def __init__(self, *args):
        super(TestOpenPose, self).__init__(*args)
        self.kTestFolder = os.path.join(file_path, "data")
        self.img_topic = "/head_front_camera/color/image_raw/compressed"
        self.bbox_topic = "/vision_msgs/mask_detections"
        self.img_path = os.path.join(self.kTestFolder, "test_image.jpg")
        self.bbox_path = os.path.join(self.kTestFolder, "bboxes_test_image.npy")
        self.img_publisher = rospy.Publisher(self.img_topic, CompressedImage, queue_size=5)
        self.bbox_publisher = rospy.Publisher(self.bbox_topic, BoxDetectionFrame, queue_size=5)

        self.persons = None

    def draw_rect(self, img):
        img_ = img.copy()
        for bbox in self.bboxes:
            bbox[2] -= bbox[0]
            bbox[3] -= bbox[1]
            img_ = cv2.rectangle(img_, bbox, (0, 166, 0), 2)
        return img_

    def _callback(self, ros_data):
        rospy.loginfo("message received")
        rospy.loginfo(ros_data)
        self.persons = ros_data.persons

    def wait_for_msg(self, timeout=3.0):
        end = rospy.Time.now() + rospy.Duration(timeout)
        while rospy.Time.now() < end:
            if self.persons is not None:
                return
            rospy.sleep(0.1)
        raise rospy.ROSException("[OPENPOSE UNITTEST] Timeout waiting for msg")

    def publish(self):
        try:
            img = cv2.imread(self.img_path)
            rospy.loginfo(f"{self.img_path}")
            rospy.loginfo(img.shape)
        except:
            print(f"[OPENPOSE Test] Error: Could not find {self.img_path}")
            return
        try:
            bbox = np.load(self.bbox_path)
            rospy.loginfo(bbox[0])
        except:
            print(f"[OPENPOSE Test] Error: Could not find {self.bbox_path}")
            return
        img_msg = CompressedImage()
        img_msg.header.stamp = rospy.Time.now()
        img_msg.format = "jpeg"
        img_msg.data = np.array(cv2.imencode(".jpg", img)[1]).tobytes()

        bbox_msg = BoxDetectionFrame()
        bbox_msg.header.stamp = img_msg.header.stamp

        def make_box(bbox):
            msg = BoxDetection()
            msg.class_id = 1
            msg.conf_score = 1
            msg.bbox = bbox
            return msg

        bbox_msg.bboxes = [make_box(x) for x in bbox]

        self.img_publisher.publish(img_msg)
        self.bbox_publisher.publish(bbox_msg)


if __name__ == "__main__":
    import rostest

    rospy.init_node("ros_openpose_test", log_level=rospy.INFO)
    rostest.rosrun("ros_openpose", "test_ros_openpose", TestOpenPose)
