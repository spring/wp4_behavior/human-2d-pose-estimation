#!/bin/bash

set -e

PATH="/opt/ros/noetic/bin:$PATH"

source /opt/ros/noetic/setup.bash
source /home/spring_ws/devel/setup.bash

if [ "$1" = "run" ]; then
    exec roslaunch ros_openpose run.launch
fi

if [ "$1" = "basestation" ]; then
    exec roslaunch ros_openpose run.launch input_topic:=/head_front_basestation/image_raw/compressed
fi

exec "$@"
